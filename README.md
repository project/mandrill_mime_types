# Mandrill Mime Types

This module allows you to manage the mimetypes the Mandrill module can send from the UI.

Mandrill currently only allows mimetypes to be added through a hook.  No the mimetypes can be managed from:

admin/config/services/mandrill/mimetypes

The values checked in the config will replace all values set in Mandrill.  It applies to all emails sent.
