<?php

/**
 * @file
 * Defines features and functions for Mandill Mime Type module.
 */

/**
 * Implements hook_menu().
 */
function mandrill_mime_types_menu() {
  $items['admin/config/services/mandrill/mimetypes'] = array(
    'title' => 'File Types',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('mandrill_mime_types_settings_form'),
    'access arguments' => array('administer mandrill'),
    'description' => 'Edit allowed mandrill mime types for this site.',
    'type' => MENU_LOCAL_TASK,
    'weight' => 2,
  );
  return $items;
}

/**
 * Settings form for mime types.
 */
function mandrill_mime_types_settings_form($form, &$form_state) {
  include_once DRUPAL_ROOT . '/includes/file.mimetypes.inc';
  $set_types = variable_get('mandrill_mime_types', mandrill_mime_types_get_mandrill_defaults());
  $mime_types = file_mimetype_mapping();
  $extensions = array_flip($mime_types['extensions']);
  $extensions['zero'] = $extensions[0];
  unset($extensions[0]);
  $form = array();
  $form['mandrill_mime_type_intro'] = array(
    '#title' => t('Mandrill Mime Types'),
    '#markup' => t('Select all file types you want to enable to be sent through Mandrill.'),
  );
  $form['mandrill_mime_types'] = array(
    '#title' => t('Allowed mime types:'),
    '#type' => 'checkboxes',
    '#options' => $extensions,
    '#default_value' => $set_types,
    '#suffix' => '<div class="mandrill-mime-types-clear"></div>',
  );
  drupal_add_css(drupal_get_path('module', 'mandrill_mime_types') . '/styles/styles.css');
  return system_settings_form($form);
}


/**
 * Implements hook_mandrill_valid_attachment_types_alter().
 */
function mandrill_mime_types_mandrill_valid_attachment_types_alter(&$types) {
  $mime_types = file_mimetype_mapping();
  $set_types = variable_get('mandrill_mime_types', array());
  $new_types = array();
  foreach ($set_types as $type_number) {
    if ($type_number !== 0 && isset($mime_types['mimetypes'][$type_number])) {
      $new_types[] = $mime_types['mimetypes'][$type_number];
    }
    if ($type_number === 'zero') {
      $new_types[] = $mime_types['mimetypes'][0];
    }
  }
  $types = $new_types;
}

/**
 * Get the Mandrill defaults
 */

function mandrill_mime_types_get_mandrill_defaults(){
    $mime_types = file_mimetype_mapping();
    $defaults = array();
    $default_types = array('application/pdf', 'application/zip');
    foreach($default_types as $type){
        if($id = array_search($type, $mime_types['mimetypes'])){
            $defaults[$id] = $id;
        }
    }
    $default_categories = array(
        'image',
        'text'
    );
    foreach($mime_types['mimetypes'] as $key  => $type){
        $type_exploded = explode('/', $type);
        if(in_array($type_exploded[0],$default_categories)){
            $defaults[$key] = $key;
        }
    }
    return $defaults;
}
